const port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
    ip = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';

const express = require('express');
const req = require('request');
const app = express();
const parser = require('xml2json');
var bodyParser = require('body-parser')
app.use(bodyParser.json())

const teamsOptions = {
    url: 'http://api.football-data.org/v1/competitions/467/teams',
    headers: {
        'X-Auth-Token': '5999f030aaea4506be580a60407dbb41'
    },
    json: true
};

app.get('/teams', (request, response) => {
    req(options, function (error, resp, body) {
        if (error) {
            return console.log(error);
        }
        response.set('Cache-Control', 'public, max-age=300, s-maxage=600');
        response.send(body);
    });
});

let artilheiros = [];

app.get('/artilheiros', (request, response) => {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.send(artilheiros);
});

app.post('/artilheiros', (request, response) => {
    response.setHeader('Access-Control-Allow-Origin', '*');
    artilheiros = request.body;
    response.send(artilheiros);
});

const fixturesOptions = {
    url: 'http://api.football-data.org/v1/competitions/467/fixtures',
    headers: {
        'X-Auth-Token': '5999f030aaea4506be580a60407dbb41',
        'Access-Control-Allow-Origin': '*'
    },
    json: true
};

let ultimaConsulta;
let resultadoConsulta;

app.get('/fixtures', (request, response) => {

    response.setHeader('Access-Control-Allow-Origin', '*');

    var time = new Date().getHours() + ":" + new Date().getMinutes();

    if (time == ultimaConsulta) {
        response.send(resultadoConsulta);
    } else {
        req(fixturesOptions, function (error, resp, body) {
            if (error) {
                return console.log(error);
            }
            ultimaConsulta = time;
            console.log('última consulta => ' + ultimaConsulta);
            resultadoConsulta = body;
            response.send(body);
        });
    }
});

let items = [{
    posicao: 1,
    team: "ARA",
    grupo: "Grupo A",
    image: "arabiasaudita",
    pts: 0,
    jogos: 0,
    vitorias: 0,
    empates: 0,
    derrotas: 0,
    golspro: 0,
    golscontra: 0,
    saldo: 0
},
    {
        posicao: 2,
        team: "EGI",
        grupo: "Grupo A",
        image: "egito",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "RUS",
        grupo: "Grupo A",
        image: "russia",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "URU",
        grupo: "Grupo A",
        image: "uruguai",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "ESP",
        grupo: "Grupo B",
        image: "espanha",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "IRA",
        grupo: "Grupo B",
        image: "ira",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "MAR",
        grupo: "Grupo B",
        image: "marrocos",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "POR",
        grupo: "Grupo B",
        image: "portugal",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "AUS",
        grupo: "Grupo C",
        image: "australia",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "DIN",
        grupo: "Grupo C",
        image: "dinamarca",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "FRA",
        grupo: "Grupo C",
        image: "franca",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "PER",
        grupo: "Grupo C",
        image: "peru",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "ARG",
        grupo: "Grupo D",
        image: "argentina",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "CRO",
        grupo: "Grupo D",
        image: "croacia",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "ISL",
        grupo: "Grupo D",
        image: "islandia",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "NIG",
        grupo: "Grupo D",
        image: "nigeria",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "BRA",
        grupo: "Grupo E",
        image: "brasil",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "COS",
        grupo: "Grupo E",
        image: "costarica",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "SER",
        grupo: "Grupo E",
        image: "servia",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "SUI",
        grupo: "Grupo E",
        image: "suica",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "ALE",
        grupo: "Grupo F",
        image: "alemanha",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "COR",
        grupo: "Grupo F",
        image: "coreia",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "MEX",
        grupo: "Grupo F",
        image: "mexico",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "SUE",
        grupo: "Grupo F",
        image: "suecia",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    }

    ,
    {
        posicao: 2,
        team: "BEL",
        grupo: "Grupo G",
        image: "belgica",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "ING",
        grupo: "Grupo G",
        image: "inglaterra",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "PAN",
        grupo: "Grupo G",
        image: "panama",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "TUN",
        grupo: "Grupo G",
        image: "tunisia",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "COL",
        grupo: "Grupo H",
        image: "colombia",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "JAP",
        grupo: "Grupo H",
        image: "japao",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "POL",
        grupo: "Grupo H",
        image: "polonia",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    },
    {
        posicao: 2,
        team: "SEN",
        grupo: "Grupo H",
        image: "senegal",
        pts: 0,
        jogos: 0,
        vitorias: 0,
        empates: 0,
        derrotas: 0,
        golspro: 0,
        golscontra: 0,
        saldo: 0
    }];


app.get('/classificacao', (request, response) => {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.send(items);
});

let current = {
    "current": 2
};

app.get('/current', (request, response) => {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.send(current);
});


app.post('/classificacao', (request, response) => {
    response.setHeader('Access-Control-Allow-Origin', '*');
    items = request.body;
    response.send(items);
});

app.post('/current', (request, response) => {
    response.setHeader('Access-Control-Allow-Origin', '*');
    current = request.body;
    response.send(current);
});






const xmlOptions = {
    url: 'http://www.xmlsoccer.com/FootballDataDemo.asmx/GetAllLeagues?ApiKey=DVQNLGQOJZTHDOHSDNQZRUQIBBJPVWKHLGEHPIDMVNDZFOREHZ',
    json: true
};

app.get('/xmlsoccer', (request, response) => {

    response.setHeader('Access-Control-Allow-Origin', '*');

    req(xmlOptions, function (error, resp, body) {
        if (error) {
            return console.log(error);
        }
        response.send(parser.toJson(body));
    });
});


app.listen(port, ip);
console.log('Server running on http://%s:%s', ip, port);

module.exports = app ;


//http://www.xmlsoccer.com/FootballDataDemo.asmx/GetAllLeagues?ApiKey=DVQNLGQOJZTHDOHSDNQZRUQIBBJPVWKHLGEHPIDMVNDZFOREHZ